<%--
  Created by IntelliJ IDEA.
  User: edena
  Date: 13.04.2024
  Time: 12:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Registration Form</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<div class="container">
  <h2>Registration Form</h2>
  <form action="${pageContext.request.contextPath}/register" method="post">
    <div class="form-group">
      <label>Name</label>
      <input type="text" name="name" class="form-control">
      <c:if test="${not empty errorMessages.name}">
        <p>${errorMessages.name}</p>
      </c:if>
    </div>
    <div class="form-group">
      <label>Email</label>
      <input type="text" name="email" class="form-control">
      <c:if test="${not empty errorMessages.email}">
        <p>${errorMessages.email}</p>
      </c:if>
    </div>
    <div class="form-group">
      <label>Age</label>
      <input type="number" name="age" class="form-control">
      <c:if test="${not empty errorMessages.age}">
        <p>${errorMessages.age}</p>
      </c:if>
    </div>
    <div class="form-group">
      <label>Birthdate</label>
      <input type="date" name="birthdate" class="form-control">
      <c:if test="${not empty errorMessages.birthdate}">
        <p>${errorMessages.birthdate}</p>
      </c:if>
    </div>
    <div class="form-group">
      <label>Phone Number</label>
      <input type="text" name="phoneNumber" class="form-control">
      <c:if test="${not empty errorMessages.phoneNumber}">
        <p>${errorMessages.phoneNumber}</p>
      </c:if>
    </div>
    <div class="form-group">
      <label>About Me</label>
      <textarea name="aboutMe" class="form-control"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
</body>
</html>


